<?php

namespace App\Http\Controllers;

use App\Msvalue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex(){
        return view('dashboard.index');
    }

    public function getLogout(){
        Auth::logout();

        alert()->success('You have been logged out.', 'Good bye!');

        return redirect('/');
    }

    public function postSaveValue(Request $request){
        $validatedData = $request->validate([
            'value' => 'required|max:50|alpha_num'
        ]);

        $msvalue = new Msvalue();
        $msvalue->value = $request->input('value');
        $msvalue->save();

        alert()->success('Data saved', 'Thanks!');

        return redirect()->back();

    }
}
